


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

#define PIN            6
#define NUMPIXELS      16
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int incomingByte = 0;   // for incoming serial data
int delayval = 500; // delay for half a second
char full[96];
char hexeach[6];
char redst[4];
char bluest[4];
char greenst[4];
int i, j;
bool continua=true;



void setup() {
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
  Serial.begin(9600);
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
  // End of trinket special code

  pixels.begin(); // This initializes the NeoPixel library.
}
void loop() {


  // send data only when you receive data:
  if (Serial.available() > 0) {
    // read the incoming byte:
    i = 0;
    comeco:
    incomingByte = Serial.read();
    while (i < 96&&incomingByte!=10) {
      while(Serial.available()==0){}
      //Serial.print("I received: ");
      //Serial.print(incomingByte,HEX);
      full[i] = incomingByte;
      incomingByte = Serial.read();
      i++;
      
    }
    
    //Serial.print("TAMANHO DA STRING EH PRA SER 288: ");
    // Serial.println(i);
    full[96] = '\0';
    //Serial.print("-");
     //Serial.println(full);
    for (i = 0; i < 96; i += 6) {
      for (j = 0; j < 6; j++) {
        hexeach[j] = full[i + j];
      }
      hexeach[6] = '\0';
      long number = strtol( &hexeach[0], NULL, 16);

      //Serial.print("=");
      //Serial.print(hexeach);

      // Split them up into r, g, b values
      long r = number >> 16;
      long g = number >> 8 & 0xFF;
      long b = number & 0xFF;

      int redv = (int)r;
      int greenv = (int)g;
      int bluev = (int)b;
      pixels.setPixelColor((i+1) / 6, pixels.Color(redv, greenv, bluev)); // Moderately bright green color.
      pixels.show();
    }

    
  }


}

